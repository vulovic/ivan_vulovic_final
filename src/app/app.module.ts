import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";

import { MDBBootstrapModule } from 'angular-bootstrap-md';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { ContactsComponent } from './components/contacts/contacts.component';
import { NewContactComponent } from './components/new-contact/new-contact.component';
import { ContactComponent } from './components/contact/contact.component';
import { EditContactComponent } from './components/edit-contact/edit-contact.component';
import { MenuComponent } from './components/menu/menu.component';
import { ContactModel } from './models/contact.model';
import { ContactService } from './services/contact.service';
import { SearchPipe } from './pipes/search.pipe';


import { LoadingModule, ANIMATION_TYPES } from 'ngx-loading';
import { FooterComponent } from './components/footer/footer.component';
import { MenuModel } from './models/menu.model';

import { MyDatePickerModule } from '../../node_modules/angular4-datepicker/src/my-date-picker/my-date-picker.module';
import { CreateUserComponent } from './components/create-user/create-user.component';


const routes:Routes = [
  {path: '', component: HomeComponent},
  {path: 'contacts', component: ContactsComponent},
  {path: 'contacts/new', component: NewContactComponent},
  {path: 'contact/:id', component: ContactComponent},
  {path: 'contact/:id/edit', component: EditContactComponent},
  {path: 'users/new', component: CreateUserComponent}
]


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ContactsComponent,
    NewContactComponent,
    ContactComponent,
    EditContactComponent,
    MenuComponent,
    SearchPipe,
    FooterComponent,
    CreateUserComponent
    
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes), 
    MDBBootstrapModule.forRoot(),
    ReactiveFormsModule,
    HttpModule,
    FormsModule,
    LoadingModule.forRoot({
      animationType: ANIMATION_TYPES.wanderingCubes,
      backdropBackgroundColour: 'rgba(0,0,0,0.1)', 
      backdropBorderRadius: '4px',
      primaryColour: '#777', 
      secondaryColour: '#777', 
      tertiaryColour: '#777'
  }),
    MyDatePickerModule
  ],
  providers: [ContactModel, ContactService, MenuModel],
  bootstrap: [AppComponent]
})
export class AppModule { }
