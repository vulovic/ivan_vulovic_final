import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(values: any, searchQuery: any): any {
    return searchQuery ? values.filter((contact) => String(contact['firstName']).toLowerCase().indexOf(searchQuery.toLowerCase()) > -1 ) : values;
  }

}
