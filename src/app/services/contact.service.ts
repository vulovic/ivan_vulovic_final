import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import "rxjs/add/operator/map";


@Injectable()
export class ContactService {

  constructor(private http:Http) { }

  getContacts(){
    return this.http.get("http://localhost:3000/contacts").map(res => res.json());
  }

  createContact(data){
    return this.http.post("http://localhost:3000/contacts", data).map(res => res.json());
  }

  getContact(id){
    return this.http.get("http://localhost:3000/contacts/"+id).map(res => res.json());    
  }

  editContact(data){
    return this.http.patch("http://localhost:3000/contacts/"+data.id, data).map(res => res.json());
  }

  deleteContact(id){
    return this.http.delete("http://localhost:3000/contacts/"+id).map(res => res.json());
  }

}
