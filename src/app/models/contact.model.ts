import { Injectable } from '@angular/core';
import { ContactService } from '../services/contact.service';

@Injectable()
export class ContactModel {

  contacts = [];
  contact = {};

  public loading = false;

  constructor(private contactService:ContactService) {
      this.getContacts();
   }


  getContacts(){
    this.loading = true;
    this.contactService.getContacts().subscribe(contacts => {
      this.contacts = contacts;
      this.loading = false; 
    })
  }

  createContact(data, clbk){
    this.contactService.createContact(data).subscribe(contact =>{
      this.contacts.push(contact)
      clbk();
    });
  }

  getContact(id, clbk){
    this.contactService.getContact(id).subscribe(contact=>{
      this.contact = contact;
      clbk(contact);  
    });
  }

  editContact(data, clbk){
    this.contactService.editContact(data).subscribe(contact=>{
      var i = 0;
      for(let current of this.contacts){
           this.contacts[i] = parseInt(current.id) === parseInt(contact.id) ? contact : current;
           i++;
      }
      clbk();
    });
  }
 
  deleteContact(id, clbk){
    this.contactService.deleteContact(id).subscribe(() => {
        this.getContacts();
        clbk();
    });
  }
}
