import { Injectable } from '@angular/core';

@Injectable()
export class MenuModel {

  links = [
    {path:'/',label:'Home'},
    {path:'/contacts',label:'Contacts'},
    {path:'/contacts/new',label:'Create contact'},
    {path:'/users/new',label:'Create user'}
  ]

  constructor() { }

}
