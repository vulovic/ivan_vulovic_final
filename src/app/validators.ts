import { FormControl, ValidatorFn, AbstractControl, Validators } from "@angular/forms";

export function validateDate(): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} => {
    if(control.value){
      if(control.value.from || control.value.to){
        return control.value.from > control.value.to ? {} : {dateError: true} ;
      }
    }
  }
}

export function validateAddress(): ValidatorFn {
  return (control: AbstractControl) : {[key: string]: any} => {
    if(control.value){
      
      let {city, street, streetNumber}  = control.value;
      
      if(city && street && streetNumber){
        return;
      }     

      if(city || street || streetNumber){
        return {addressError: true}
      }

    }
  }
}