import { Component, OnInit } from '@angular/core';
import { MenuModel } from '../../models/menu.model';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html'
})
export class MenuComponent{

  constructor(private menuModel:MenuModel) { }

}
