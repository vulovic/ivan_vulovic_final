import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ContactModel } from '../../models/contact.model';
import { Router } from '@angular/router';
import { IMyDpOptions } from "../../../../node_modules/angular4-datepicker/src/my-date-picker";

@Component({
  selector: 'app-new-contact',
  templateUrl: './new-contact.component.html'
})
export class NewContactComponent {
    
  submitTried = false;

  form: FormGroup;

  options: IMyDpOptions = {
    dateFormat: "dd/mm/yyyy",
    openSelectorOnInputClick: true,
    editableDateField: false,
    inline: false
  };


  constructor(private contactModel:ContactModel, private router:Router) { 

    this.form = new FormGroup({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      photo: new FormControl('', Validators.required),
      age: new FormControl(''),      
      birthDate: new FormControl('')
    })
  }



  submit(){
    this.submitTried = true;

    if(this.form.valid){
      var status = false;
      var data = {age};
      var timestamp = 0;
      var age = 0;      

      var formAge = this.form.value.age;
      var formBirthDate = this.form.value.birthDate;

      if(formAge == '' && formBirthDate == ''){
         alert("Please enter age, birth date, or enter both.");
      }
      if(formAge || formBirthDate){
        timestamp = this.form.value.birthDate.jsdate && +new Date(this.form.value.birthDate.jsdate);
        age = formAge && formAge;
        if(timestamp > 0){
          data = {...this.form.value, birthDate: timestamp, age:0}
          status = true;
        }        
        if(age > 0){
          data = {...this.form.value, age:age, birthDate: "0"}
          status = true;          
        }
        if(timestamp>0 && age>0){
          data = {...this.form.value, birthDate: timestamp, age: age}     
          let ageDiff = new Date().getFullYear() - new Date(+new Date(this.form.value.birthDate.jsdate)).getFullYear();
          if( ageDiff == data.age){
            status = true;          
          } else {
            status = false;
            alert("Please check the age. If you are filling out both age and date of birth make sure that they match. Note that age has to be greater than 0.");
          }
        }
        if(status){
          alert("Contact added!");
          this.contactModel.createContact(data, ()=>this.router.navigate(['/contacts']));
        }
      }
    }    
  }
}
