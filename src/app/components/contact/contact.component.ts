import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ContactModel } from '../../models/contact.model';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html'
})
export class ContactComponent implements OnInit, OnDestroy {

  id;

  constructor(private route:ActivatedRoute, private contactModel: ContactModel, private router:Router) {}

  ngOnInit() {
    this.route.params.subscribe((params)=>this.id = params.id);
    this.contactModel.getContact(this.id, ()=>{});   
  }

  deleteContact(id){
    this.contactModel.deleteContact(this.id, ()=>this.router.navigate(['/contacts']));
  }

  ngOnDestroy(){
    this.contactModel.contact = {};
  }

}
