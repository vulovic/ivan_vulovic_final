import { Component, OnInit } from '@angular/core';
import { MenuModel } from '../../models/menu.model';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html'
})
export class FooterComponent implements OnInit {

  constructor(private menuModel:MenuModel) { }

  ngOnInit() {
  }

}
