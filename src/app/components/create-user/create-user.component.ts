import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { validateDate, validateAddress } from '../../validators';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html'
})
export class CreateUserComponent {

  form: FormGroup;
  
  constructor(private fb: FormBuilder) { 
    
    this.form = this.fb.group({
      firstName: ['', [Validators.required]],
      address: this.fb.group({
        city: [undefined,[]],
        street: [undefined,[]],
        streetNumber: [undefined,[]],
      }),
      date: this.fb.group({
        from: [undefined, []],
        to: [undefined, []]
      })
    })

    //this.onFieldChange(this.form, "address");

    this.form.get("address").setValidators(validateAddress());
    this.form.get("date").setValidators(validateDate());
    
  }

  /*
  addressHasValidators = false;

  
  onFieldChange(form, field){

    // If one field is filled out then all fields are required. If none is filled none of the fields in the address group are required.

    form.get(field).valueChanges.subscribe((values) => {
      
      let {city, street, streetNumber}  = values;
      
      if((city || street || streetNumber) && !this.addressHasValidators){
        this.addressHasValidators = true;
        for(let key in values){
          form.get(`${field}.${key}`).setValidators(Validators.required);
          form.get(`${field}.${key}`).updateValueAndValidity();
        }
      }
      
      if((!city && !street && !streetNumber) && this.addressHasValidators){
        this.addressHasValidators = false;
        for(let key in values){
          form.get(`${field}.${key}`).setValidators([]);
          form.get(`${field}.${key}`).updateValueAndValidity();          
        }
      }      

    });

  }
  
  */

  submit(){}

}
