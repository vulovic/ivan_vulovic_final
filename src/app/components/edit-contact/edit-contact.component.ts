import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ContactModel } from '../../models/contact.model';
import { Router, ActivatedRoute } from '@angular/router';
import { IMyDpOptions } from "../../../../node_modules/angular4-datepicker/src/my-date-picker";

@Component({
  selector: 'app-edit-contact',
  templateUrl: './edit-contact.component.html'
})
export class EditContactComponent{

  id;
  submitTried = false;
  values = {};

  form: FormGroup;
  options: IMyDpOptions = {
    dateFormat: "dd/mm/yyyy",
    openSelectorOnInputClick: true,
    editableDateField: false,
    inline: false,
  };

  constructor(private route:ActivatedRoute, private contactModel:ContactModel, private router:Router) { 
    this.form = new FormGroup({
      firstName: new FormControl({value: '', disabled: true}, Validators.required),
      lastName: new FormControl({value: '', disabled: true}, Validators.required),
      photo: new FormControl('', Validators.required),
      age: new FormControl(''),      
      birthDate: new FormControl('')
    })
    this.route.params.subscribe((params)=>this.id = params.id);       
    this.contactModel.getContact(this.id, (result) => this.getValues(result));
  }
  getValues(result){
    var date = new Date(result.birthDate);
    let initialValues = {
      ...result,
      birthDate: {
        date: {
          day: date.getDate(),
          month: date.getMonth() + 1,
          year: date.getFullYear()
        }
      }
    };
    delete initialValues.id;
    this.form.setValue(initialValues);
    result.birthDate == 0 && this.form.patchValue({birthDate: null});
    result.age == 0 && this.form.patchValue({age: null});
  }

  submit(){ 
    this.submitTried = true;

    if(this.form.valid){
      var status = false;
      var data = {age};
      var timestamp = 0;
      var age = 0;      

      var formAge = this.form.value.age;
      var formBirthDate = this.form.value.birthDate;

      if(formAge == '' && formBirthDate == ''){
         alert("Please enter age, birth date, or enter both.");
      }
      if(formAge || formBirthDate){
        timestamp = formBirthDate && +new Date(formBirthDate.jsdate);
        age = formAge && formAge;
        if(timestamp > 0){
          data = {...this.form.value, birthDate: timestamp, age:0}
          status = true;
        }        
        if(age > 0){
          data = {...this.form.value, age:age, birthDate: "0"}
          status = true;          
        }
        if(timestamp>0 && age>0){
          data = {...this.form.value, birthDate: timestamp, age: age}     
          var ageDiff = new Date().getFullYear() - new Date(Number(new Date(this.form.value.birthDate.jsdate))).getFullYear();
          if(ageDiff == data.age){
            status = true;          
          } else {
            status = false;
            alert("Please check the age. If you are filling out both age and date of birth make sure that they match. Note that age has to be greater than 0.");
          }
        }
        if(status){
          let birth= formBirthDate == null ? '0' : +new Date(formBirthDate.jsdate);
          alert("Contact edited!");
          let editData = {id: this.id, ...this.form.value, birthDate: birth};
          this.contactModel.editContact(editData, ()=>this.router.navigate(['/contacts']));
        }
      }
    }   
  }

}
